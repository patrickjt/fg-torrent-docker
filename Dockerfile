FROM alpine

RUN apk update \
	&& apk add build-base automake autoconf cmake curl curl-dev libcurl libevent libevent-dev git openssl openssl-dev libtool intltool \
	&& rm -rf /var/cache/apk/*

RUN git clone https://github.com/transmission/transmission \
	&& cd transmission \
	&& git checkout gh305-improve-dotfiles \
	&& git submodule update --init \
	&& mkdir build \
	&& cd build \
	&& cmake .. \
	&& make \
	&& make install
